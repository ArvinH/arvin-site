var express = require('express');
var router = express.Router();
var fbBotUtil = require('./lib/fbbot.js');
var gag = require('node-9gag');
var fetchr = require('./lib/fetchr.js');
var WitTOKEN = require('./lib/bot_oil.js').witToken;

let Wit = null;
try {
  // if running from repo
  Wit = require('../').Wit;
} catch (e) {
  Wit = require('node-wit').Wit;
}

const sessions = {};

const findOrCreateSession = (fbid) => {
  let sessionId;
  // Let's see if we already have a session for the user fbid
  Object.keys(sessions).forEach(k => {
    if (sessions[k].fbid === fbid) {
      // Yep, got it!
      sessionId = k;
    }
  });
  if (!sessionId) {
    // No session found for user fbid, let's create a new one
    sessionId = new Date().toISOString();
    sessions[sessionId] = {fbid: fbid, context: {}};
  }
  return sessionId;
};

const firstEntityValue = (entities, entity) => {
  const val = entities && entities[entity] &&
    Array.isArray(entities[entity]) &&
    entities[entity].length > 0 &&
    entities[entity][0].value
  ;
  if (!val) {
    return null;
  }
  return typeof val === 'object' ? val.value : val;
};

// Our bot actions
const actions = {
  send(request, response) {

  	const {sessionId, context, entities} = request;
    let {text, quickreplies} = response;
    var greeting = firstEntityValue(entities, 'greeting');
    text = (greeting) ? (greeting + text) : text;
    // Our bot has something to say!
    // Let's retrieve the Facebook user whose session belongs to
    const recipientId = sessions[sessionId].fbid;
    if (recipientId) {
    	
      // Yay, we found our recipient!
      // Let's forward our bot response to her.
      // We return a promise to let our bot know when we're done sending
      if (context.newsResult && !greeting) {
      	return fbBotUtil.sendNewsMessagePromise(recipientId, context.newsResult)
		      .then(() => null)
		      .catch((err) => {
		        console.error(
		          'Oops! An error occurred while forwarding the response to',
		          recipientId,
		          ':',
		          err.stack || err
		        );
		      });

		  } else {

		    	return fbBotUtil.sendTextMessagePromise(recipientId, text)
			      .then(() => null)
			      .catch((err) => {
			        console.error(
			          'Oops! An error occurred while forwarding the response to',
			          recipientId,
			          ':',
			          err.stack || err
			        );
			      });
		    }
      
    } else {
      console.error('Oops! Couldn\'t find user for session:', sessionId);
      // Giving the wheel back to our bot
      return Promise.resolve()
    }
  },
  getNews({context, entities}) {
    return new Promise(function(resolve, reject) {
      var search_query = firstEntityValue(entities, 'search_query');
      var greeting = firstEntityValue(entities, 'greeting');
      var abilityQuestion = firstEntityValue(entities, 'abilityQuestion');
      if (greeting) {
        delete context.missNews;
        delete context.newsResult;
        return resolve(context);
      } else if (abilityQuestion) {
        delete context.missNews;
        delete context.newsResult;
        return resolve(context);
      } else if (search_query) {
      	fetchr(search_query, function(data) {
					context.newsResult = data;
					console.log('context newsResult', context.newsResult);
					delete context.missNews;
          return resolve(context);
				});
        
      } else {
				context.missNews = true;
				delete context.newsResult;
        return resolve(context);
      }
      
    });
  },
};

const wit = new Wit({
  accessToken: WitTOKEN,
  actions,
});

router.get('/', function (req, res) {
	if (req.query['hub.verify_token'] === 'yucc') {
		res.send(req.query['hub.challenge']);
	} else {
		res.send('Error, wrong validation token');
	}	
})

router.post('/', function (req, res) {
	messaging_events = req.body.entry[0].messaging;
	for (i = 0; i < messaging_events.length; i++) {
		event = req.body.entry[0].messaging[i];
		sender = event.sender.id;
		var sessionId = findOrCreateSession(sender);
		if (event.message && event.message.text) {
			text = event.message.text;
			// Handle a text message from this sender
			console.log('user text', text);
			if (text === 'Generic') {
				fbBotUtil.sendGenericMessage(sender);
			} else if (text === 'gag') {
				gag.section('funny', 'hot', function (err, res) {
				  fbBotUtil.sendGagMessage(sender, res);	
				});
			} else {
				
        wit.runActions(
          sessionId, // the user's current session
          text, // the user's message
          sessions[sessionId].context // the user's current session state
        ).then((context) => {
          // Our bot did everything it has to do.
          // Now it's waiting for further messages to proceed.
          console.log('Waiting for next user messages');
          console.log('user context', context);
          // Updating the user's current session state
          if (context.newsResult) {
            delete context.newsResult;
          }
          sessions[sessionId].context = context;
        })
        .catch((err) => {
          console.error('Oops! Got an error from Wit: ', err.stack || err);
        })

				/*
				fbBotUtil.sendTextMessage(sender, '正在為您搜尋'+text+'的相關新聞...');
				fetchr(text, function(data) {
					fbBotUtil.sendNewsMessage(sender, data);
				});
				*/
			}
			
	  }
	}
  res.sendStatus(200);
});

module.exports = router;
