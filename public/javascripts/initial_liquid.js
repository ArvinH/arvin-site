var config1 = liquidFillGaugeDefaultSettings();
config1.circleColor = '#12e5e7';
config1.textColor = '#5efdff';
config1.waveTextColor = '#3facaf';
config1.waveColor = '#5efdff';
config1.textVertPosition = 0.2;
config1.waveAnimateTime = 1000;
var config2 = liquidFillGaugeDefaultSettings();
config2.circleColor = '#71ffa6';
config2.textColor = '#3de320';
config2.waveTextColor = '#21960d';
config2.waveColor = '#3de320';
config2.textVertPosition = 0.2;
config2.waveAnimateTime = 1000;
var config3 = liquidFillGaugeDefaultSettings();
config3.circleColor = '#feff2c';
config3.textColor = '#fffc43';
config3.waveTextColor = '#a7a431';
config3.waveColor = '#fffc43';
config3.textVertPosition = 0.2;
config3.waveAnimateTime = 1000;
var config4 = liquidFillGaugeDefaultSettings();
config4.circleColor = '#eb796e';
config4.textColor = '#ff2124';
config4.waveTextColor = '#FFAAAA';
config4.waveColor = '#ff2124';
config4.textVertPosition = 0.2;
config4.waveAnimateTime = 1000;

var config5 = liquidFillGaugeDefaultSettings();
config5.circleColor = '#feff2c';
config5.textColor = '#fffc43';
config5.waveTextColor = '#a7a431';
config5.waveColor = '#fffc43';
config5.textVertPosition = 0.2;
config5.waveAnimateTime = 1000;
config5.minValue = 0;
config5.maxValue = 120;
config5.displayPercent = false;

var config6 = liquidFillGaugeDefaultSettings();
config6.circleColor = '#eb796e';
config6.textColor = '#ff2124';
config6.waveTextColor = '#FFAAAA';
config6.waveColor = '#ff2124';
config6.textVertPosition = 0.2;
config6.waveAnimateTime = 1000;
config6.minValue = 0;
config6.maxValue = 990;
config6.displayPercent = false;

var gauge1 = loadLiquidFillGauge('fillgauge1', 70, config1);
var gauge2 = loadLiquidFillGauge('fillgauge2', 50, config2);
var gauge3 = loadLiquidFillGauge('fillgauge3', 70, config3);
var gauge4 = loadLiquidFillGauge('fillgauge4', 60, config4);
var gauge5 = loadLiquidFillGauge('fillgauge5', 55, config1);
var gauge6 = loadLiquidFillGauge('fillgauge6', 45, config2);
var gauge7 = loadLiquidFillGauge('fillgauge7', 91, config5);
var gauge8 = loadLiquidFillGauge('fillgauge8', 810, config6);
function NewValue() {
  if (Math.random() > .5) {
    return Math.round(Math.random() * 100);
  } else {
    return (Math.random() * 100).toFixed(1);
  }
}
/*
onclick="gauge1.update(NewValue());"
*/
