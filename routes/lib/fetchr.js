var apikey = require('./bot_oil.js').importApiKey;
var request = require('request');

module.exports = function fetchr(keyword, callback) {

	request({
		url: 'https://extraction.import.io/query/extractor/17ce6926-464e-4840-b8f0-244ecb827a82',
		qs: {
			_apikey : apikey,
			url: 'https://tw.news.search.yahoo.com/search?p='+keyword+'&fr=uh3_news_web_gs'
		},
		method: 'GET'
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending message: ', error);
		} else if (response.body.error) {
			console.log('Error: ', response.body.error);
		}
		callback(JSON.parse(body));
	});

}
