var token = require('./bot_oil.js').token;
var request = require('request');
var fetchr = require('./fetchr');
var fetch = require('node-fetch');

exports.sendNewsMessagePromise = (id, res) => {
 	
	const resData = res.extractorData.data[0].group;

	const newResults = resData.map(function(item) {
		return {
			title: item.titleText[0].text,
			buttons: [{
				type: "web_url",
				url: item.titleLink[0].href,
				title: '看詳情',
			}]
		};
	});
	
	const Result = [];
	for (let i = 0; i < 3; i++) {
	  Result.push(newResults[Math.floor(Math.random()*10)]);
	}

	messageData = {
		"attachment": {
			"type": "template",
			"payload": {
				"template_type": "generic",
				"elements": Result
			}
		}
	};

	const body = JSON.stringify({
    recipient: { id },
    message: messageData,
  });
  const qs = 'access_token=' + encodeURIComponent(token);
  console.log('post in sendNewsMessage');
  return fetch('https://graph.facebook.com/v2.6/me/messages?' + qs, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body,
  })
  .then(rsp => rsp.json())
  .then(json => {
    if (json.error && json.error.message) {
      throw new Error(json.error.message);
    }
    return json;
  });
}

exports.sendTextMessagePromise = (id, text) => {
  const body = JSON.stringify({
    recipient: { id },
    message: { text },
  });
  const qs = 'access_token=' + encodeURIComponent(token);
  console.log('post in sendTextMessage');
  return fetch('https://graph.facebook.com/v2.6/me/messages?' + qs, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body,
  })
  .then(rsp => rsp.json)
  .then(json => {
    if (json.error && json.error.message) {
      throw new Error(json.error.message);
    }
    return json;
  });
};

exports.sendTextMessage = function sendTextMessage(sender, text) {
	messageData = {
		text:text
	}
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending message: ', error);
		} else if (response.body.error) {
			console.log('Error: ', response.body.error);
		}
	});
}

exports.sendGenericMessage = function sendGenericMessage(sender) {
	messageData = {
		"attachment": {
			"type": "template",
			"payload": {
				"template_type": "generic",
				"elements": [{
					"title": "First card",
					"subtitle": "Element #1 of an hscroll",
					"image_url": "http://messengerdemo.parseapp.com/img/rift.png",
					"buttons": [{
						"type": "web_url",
						"url": "https://www.messenger.com/",
						"title": "Web url"
					}, {
						"type": "postback",
						"title": "Postback",
						"payload": "Payload for first element in a generic bubble",
					}],
				},{
					"title": "Second card",
					"subtitle": "Element #2 of an hscroll",
					"image_url": "http://messengerdemo.parseapp.com/img/gearvr.png",
					"buttons": [{
						"type": "postback",
						"title": "Postback",
						"payload": "Payload for second element in a generic bubble",
					}],
				}]
			}
		}
	};
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending message: ', error);
		} else if (response.body.error) {
			console.log('Error: ', response.body.error);
		}
	});
}


exports.sendGagMessage = function sendGagMessage(sender, res) {
 
	var gagData = res.map(function(item) {
		return {
			title: item.title,
			image_url: item.image,
			buttons: [{
				type: "web_url",
				url: item.url,
				title: "web url"
			}]
		};
	});
	
	var Result = [];
	for (var i = 0; i < 3; i++) {
	  Result.push(gagData[Math.floor(Math.random()*10)]);
	}


	messageData = {
		"attachment": {
			"type": "template",
			"payload": {
				"template_type": "generic",
				"elements": Result 
			}
		}
	};
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending message: ', error);
		} else if (response.body.error) {
			console.log('Error: ', response.body.error);
		}
	});
}

exports.sendNewsMessage = function sendNewsMessage(sender, res) {
 
	var resData = res.extractorData.data[0].group;

	var newResults = resData.map(function(item) {
		return {
			title: item.titleText[0].text,
			buttons: [{
				type: "web_url",
				url: item.titleLink[0].href,
				title: '看詳情',
			}]
		};
	});
	
	var Result = [];
	for (var i = 0; i < 3; i++) {
	  Result.push(newResults[Math.floor(Math.random()*10)]);
	}

	messageData = {
		"attachment": {
			"type": "template",
			"payload": {
				"template_type": "generic",
				"elements": Result
			}
		}
	};
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending message: ', error);
		} else if (response.body.error) {
			console.log('Error: ', response.body.error);
		}
	});
}
