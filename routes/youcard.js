var express = require('express');
var youcardlist = require('./lib/mock/cardlist');
var youcard = require('./lib/mock/cardstack');
var router = express.Router();

/* GET youcard listing. */
router.get('/cardstack_search', function(req, res, next) {
  res.json(youcardlist);
});

router.get('/cardstack/:id', function(req, res, next) {
  res.json(youcard.cardstack.result[req.params.id]);
});

module.exports = router;
