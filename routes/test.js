var f = require('./lib/fetchr.js');
var v = require('input_validator');
f('柯文哲', function(res) {
	var validators = [{
		validator: 'limitLength',
		options: {
		  limit: 10
		}
	}]
	var resData = res.extractorData.data[0].group;
	var newResults = resData.map(function(item) {
		return {
			title: v(item.titleText[0].text, validators),
			buttons: [{
				type: "web_url",
				url: item.titleLink[0].href,
				title: v(item.titleText[0].text, validators),
			}]
		};
	}); 
	newResults.map(function(item) {
		console.log(item);
	});
});
