var gulp = require('gulp');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');


gulp.task('compress-js', function() {
  return gulp.src('public/javascripts/main-controller.js')
      .pipe(uglify())
      .pipe(gulp.dest('public/javascripts/'));
});

gulp.task('compress-css', function(){
   return gulp.src('public/stylesheets/stylish-portfolio.css')
         .pipe(uglifycss({'max-line-len': 80}))
         .pipe(gulp.dest('public/stylesheets/'));
});
